#!/usr/bin/python3

#  serial-hub: Create multiple connected serial interfaces
#  Copyright (C) 2024  ermi
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import multiprocessing
import numpy as np
import argparse
import serial
import pty
import sys
import os


def worker(objs_write, obj_read):
	try:
		while True:
			b = obj_read.read()
			for obj_write in objs_write:
				obj_write.write(b)
	except KeyboardInterrupt:
		pass

class virtual_interface(object):
	def __init__(self, name=None):
		self.master, self.slave = pty.openpty()
		self.link = name
		if name!=None:
			os.symlink(os.ttyname(self.slave), name)
		print(self.name(True))
	def __del__(self):
		if self.name!=None:
			os.unlink(self.link)
	def name(self, full=False):
		if self.link==None:
			return os.ttyname(self.slave)
		elif full:
			return self.link+' -> '+os.ttyname(self.slave)
		else:
			return self.link
	
	def write(self, data):
		os.write(self.master, data)
	def read(self, size=1):
		return os.read(self.master, size)

class serial_interface(object):
	def __init__(self, name, baud):
		self.ser = serial.Serial(name, baud)
		self.write = self.ser.write
		self.read = self.ser.read
		print(self.name())
	def __del__(self):
		self.ser.close()
	def name(self):
		return self.ser.name

def hub(**kwargs):
	baud = kwargs.get('baud', 9600)
	
	objs = [virtual_interface() for _ in range(kwargs.get('virtual', 0))]
	for name in kwargs.get('link', []):
		objs.append(virtual_interface(name))
	for dev in kwargs.get('device', []):
		objs.append(serial_interface(dev['name'], dev.get('baud', baud)))
	
	pp = [multiprocessing.Process(target=worker, args=(objs[:i]+objs[i+1:], objs[i])) for i in range(1, len(objs))]
	
	for p in pp:
		p.start()
	worker(objs[1:], objs[0])
	
	print('')

if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-v', '--virtual', type=int, default=0)
	parser.add_argument('-l', '--link'  , type=str, default=[], action='append')
	parser.add_argument('-d', '--device', type=str, default=[], action='append', nargs='+')
	parser.add_argument('-b', '--baud', type=int, default=9600)
	args = parser.parse_args()
	
	kwargs = vars(args)
	for i, dev in enumerate(kwargs['device']):
		match len(dev):
			case 1:
				kwargs['device'][i] = {'name': dev[0]}
			case 2:
				kwargs['device'][i] = {'name': dev[0], 'baud': int(dev[1])}
			case other:
				raise AssertionError
	
	hub(**kwargs)

